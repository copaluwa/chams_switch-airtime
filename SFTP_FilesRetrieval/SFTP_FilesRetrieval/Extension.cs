﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SFTP_FilesRetrieval
{
    public static class Extensions
    {
        public static string GetKeyValue(this string Key)
        {
            return ConfigurationManager.AppSettings[Key];
        }
    }
}
