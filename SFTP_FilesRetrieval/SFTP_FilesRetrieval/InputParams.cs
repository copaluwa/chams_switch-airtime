﻿
using ConsoleCommon;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SFTP_FilesRetrieval
{
    class InputParams : ParamsObject
    {
        public InputParams(string[] args)
            :base(args)
        {
            
        }

        [Switch("R")]
        public string RemotePath { get; set; }
        [Switch("D")]
        public string DestinationPath { get; set; }
        [Switch("F")]
        public string FileName { get; set; }
        [Switch("H")]
        public string HostName { get; set; }
        [Switch("U")]
        public string UserName { get; set; }
        [Switch("P")]
        public string Password { get; set; }
        [Switch("K")]
        public string HostKey { get; set; }
        [Switch("PT")]
        public string Port { get; set; }

    }
}
