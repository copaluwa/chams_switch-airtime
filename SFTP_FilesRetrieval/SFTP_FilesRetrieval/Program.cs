﻿using System;

namespace SFTP_FilesRetrieval
{
    class Program
    {
        static Logger log = new Logger();
        static void Main(string[] args)
        {
            
            
            Console.WriteLine($"Starting Applicaton @ {DateTime.Now}");
            log.Info($"Starting Applicaton @ {DateTime.Now}");

            RetrieveFiles.getfile(args);

            log.Info($"Closing Applicaton @ {DateTime.Now}");

        }
    }
}
