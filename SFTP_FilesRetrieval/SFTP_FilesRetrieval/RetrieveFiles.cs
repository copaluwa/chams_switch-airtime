﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WinSCP;

namespace SFTP_FilesRetrieval
{
    public class RetrieveFiles
    {
        static Logger log = new Logger();
        public static void getfile(string[] args)
        {
            try
            {
               
                //get input parameters
                InputParams inputParams = new InputParams(args);
                var filename = inputParams.FileName;

                log.Info($"Getting files to download");
                var allFiles = new List<string>();
                if (!(string.IsNullOrEmpty(filename)) && filename.Contains(";"))
                {
                    allFiles = (filename.Split(';')).ToList();
                }
                else if (!(string.IsNullOrEmpty(filename)))
                {
                    allFiles.Add(filename);
                }

                log.Info($"Deleting all files in destination folder");
                //clean up destinationFolder
                Array.ForEach(Directory.GetFiles(inputParams.DestinationPath), File.Delete);



                // Setup session options
                SessionOptions sessionOptions = new SessionOptions
                {
                    Protocol = Protocol.Sftp,
                    HostName = inputParams.HostName,
                    UserName = inputParams.UserName,
                    Password = inputParams.Password,
                    SshHostKeyFingerprint = inputParams.HostKey,
                    PortNumber = Convert.ToInt32(inputParams.Port)
                };

                using (Session session = new Session())
                {
                    // Connect
                    try
                    {
                        session.Open(sessionOptions);
                    }
                    catch (Exception ex)
                    {
                        if (ex.Message.ToLower().Contains("timed out"))
                        {
                            session.Open(sessionOptions);
                        };

                    }
                    session.Open(sessionOptions);

                    log.Info($"Session opened");
                    // Download files
                    TransferOptions transferOptions = new TransferOptions();
                    transferOptions.TransferMode = TransferMode.Binary;

                    TransferOperationResult transferResult;


                    log.Info($"File download started at {DateTime.Now}");
                    Console.WriteLine($"File download started at {DateTime.Now}");

                    foreach (var file in allFiles)
                    {
                        var filePath = Path.Combine(inputParams.RemotePath, file);

                        transferResult =
                       session.GetFiles(filePath, inputParams.DestinationPath, false, transferOptions);
                        //session.GetFiles("/home/chamsswitch/vtumanager.chamsswitch.com/EcoBank/29 MAY 2021 TRANSACTIONS Settlement.xlsx", @"c:\ECOBANK\Chams_CreditSwitch\", false, transferOptions);
                        //session.GetFiles("/home/EcoBank_vtu_autoreport_2020-12-01.xlsx", @"c:\ECOBANK\Chams_CreditSwitch\", false, transferOptions);



                        // Throw on any error
                        transferResult.Check();

                        // Print results
                        foreach (TransferEventArgs transfer in transferResult.Transfers)
                        {
                            log.Info($"Download of {0} succeeded {transfer.FileName}");
                            Console.WriteLine("Download of {0} succeeded", transfer.FileName);
                        }
                    }
                    Console.WriteLine($"Download Completed at  {DateTime.Now}");
                    log.Info($"Download Completed at  {DateTime.Now}");

                    // Print results

                }


            }
            catch (Exception ex)
            {

                log.Error(ex);
                Console.WriteLine("Error: {0}", ex);

            }
        }

    }
}
